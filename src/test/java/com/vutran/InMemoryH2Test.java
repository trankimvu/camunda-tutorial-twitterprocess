package com.vutran;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.junit.Assert.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class InMemoryH2Test {

    private final static Logger LOGGER = LoggerFactory.getLogger(InMemoryH2Test.class);


    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "twitter.process";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }

    /**
     * Just tests if the process definition is deployable.
     */
    @Test
    @Deployment(resources = "process.bpmn")
    public void testParsingAndDeployment() {
        // nothing is done here, as we just want to check for exceptions during deployment

    }

    @Test
    @Deployment(resources = "process.bpmn")
    public void testHappyPath() {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("content", "I'm Vu Tran. Hello Camunda");
        variables.put("approved", false);
        ProcessInstance processInstance = processEngine().getRuntimeService()
                                                                .startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);

        // Now: Drive the process by API and assert correct behavior by camunda-bpm-assert

        final TaskService taskService = processEngine().getTaskService();
        final List<Task> taskList = taskService.createTaskQuery().active().list();
        LOGGER.info("Active tasks: "+ taskList);
        taskList.forEach(task -> {
            LOGGER.info("Completing task: "+ task.getId() +", "+task.getName());
            taskService.complete(task.getId());
        });
        assertThat(processInstance).isEnded();
    }

}
